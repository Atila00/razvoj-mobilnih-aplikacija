package rs.ac.singidunum.android.vezbe3;

import java.io.Serializable;

public class Student implements Serializable {
    private String ime;
    private String prezime;
    private String brojIndexa;
    private String godStud;

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getBrojIndexa() {return brojIndexa;
    }

    public void setBrojIndexa(String brojTelefona) {
        this.brojIndexa = brojIndexa;
    }

    public String getGodStud() {
        return godStud;
    }

    public void setGodStud(String skypeId) {
        this.godStud = godStud;
    }

    public Student(){

    }

    public Student(String ime, String prezime, String brojIndexa, String godStud){
        this.ime = ime;
        this.prezime = prezime;
        this.brojIndexa = brojIndexa;
        this.godStud = godStud;
    }
}
