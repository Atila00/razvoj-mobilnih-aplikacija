package rs.ac.singidunum.android.vezbe3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    Context c;
    ArrayList<Student> studenti;
    ArrayList<Student> shownStudenti;
    LinearLayout mainLayout;
    LinearLayout subLayout;
    ArrayList<LinearLayout> children;
    EditText search;
    Button addButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        c = this;
        children = new ArrayList<>();
        studenti = (ArrayList<Student>)getIntent().getSerializableExtra("studenti");
        if(studenti == null)
            setupData();
        generateData();
        setupSearch();
        setupButton();

    }
    private void setupButton(){
        addButton = findViewById(R.id.addButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, EditStudent.class);
                i.putExtra("studenti", studenti);
                startActivity(i);
                finish();
            }
        });
    }

    private void setupSearch(){
        search = findViewById(R.id.inputSearch);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                shownStudenti = new ArrayList<Student>();
                if(search.getText().equals("")){
                    shownStudenti = studenti;
                }
                else{
                    for(Student s : studenti){
                        if(s.getIme().contains(search.getText().toString())||
                                s.getPrezime().contains(search.getText().toString()) ||
                                s.getGodStud().contains(search.getText().toString())){
                            shownStudenti.add(s);
                        }
                    }
                    clearAndGenerate();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void setupData(){
        studenti = new ArrayList<Student>();
        for(int i = 0; i < 5; i++){
            studenti.add(new Student("Student " + i, "Prezime " + i,
                    "BrojIndexa " + i, "GodinaStudija " + i));
        }
    }
    private void clearAndGenerate(){

        for(Student s : studenti){
            if(!shownStudenti.contains(s)){
                children.get(studenti.indexOf(s)).setVisibility(View.GONE);
            }
            else{
                children.get(studenti.indexOf(s)).setVisibility(View.VISIBLE);
            }
        }
    }

    //Generisemo sadrzaj
    private void generateData(){
        LayoutInflater inflater = LayoutInflater.from(this);
        mainLayout = findViewById(R.id.mainLayout);

        LinearLayout kontaktContent;
        Button delButton;
        TextView ime, prezime, index, godStud;
        ImageView slika;

        boolean color = false;
        for(final Student s : studenti){
            subLayout = (LinearLayout) inflater.inflate(R.layout.studentlayout,
                    mainLayout, false);
            children.add(subLayout);


            ime = subLayout.findViewById(R.id.labelIme);
            prezime = subLayout.findViewById(R.id.labelPrezime);
            index = subLayout.findViewById(R.id.labelIndex);
            godStud = subLayout.findViewById(R.id.labelGod);
            slika = subLayout.findViewById(R.id.imageView);
            delButton = subLayout.findViewById(R.id.delButton);
            kontaktContent = subLayout.findViewById(R.id.kontaktContent);

            kontaktContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(MainActivity.this, EditStudent.class);
                    i.putExtra("indeks", studenti.indexOf(s));
                    i.putExtra("studenti", studenti);
                    startActivity(i);
                    finish();
                }
            });

            delButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    mainLayout.removeView(children.get(studenti.indexOf(s)));
                    children.remove(studenti.indexOf(s));
                    studenti.remove(s);
                }
            });

            ime.setText(s.getIme());
            prezime.setText(s.getPrezime());
            index.setText(s.getBrojIndexa());
            godStud.setText(s.getGodStud());
            slika.setImageResource(R.drawable.aa);

            if(!color){
                color = true;
                subLayout.setBackgroundColor(Color.CYAN);
                slika.setImageResource(R.drawable.aa);
                delButton.setBackgroundColor(Color.GRAY);
            }
            else{
                color = false;
                subLayout.setBackgroundColor(Color.GRAY);
                slika.setImageResource(R.drawable.bb);
                delButton.setBackgroundColor(Color.CYAN);
            }
            mainLayout.addView(subLayout);
        }

    }
}
