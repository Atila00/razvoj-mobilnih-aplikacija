package rs.ac.singidunum.android.vezbe3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class EditStudent extends AppCompatActivity {

    private int indeks;
    private Student s;
    private ArrayList<Student> studenti;
    EditText ime, prezime, brIndexa, godStud;
    Button confirm, forfeit;
    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_student);
        studenti = (ArrayList<Student>)getIntent().getSerializableExtra("studenti");
        indeks = getIntent().getIntExtra("indeks", -1);
        if(indeks == -1){
            addStudent();
        }
        else{
            editStudent();
        }
    }

    public void addStudent(){
        ime = findViewById(R.id.editName);
        prezime = findViewById(R.id.editSurname);
        godStud = findViewById(R.id.editGodStud);
        brIndexa = findViewById(R.id.editbrindexa);
        confirm = findViewById(R.id.confirmButton);
        forfeit = findViewById(R.id.forfeitButton);


        s = new Student();
        i = new Intent(EditStudent.this, MainActivity.class);
        forfeit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                i.putExtra("studenti", studenti);
                startActivity(i);
                finish();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                s.setIme(ime.getText().toString());
                s.setPrezime(prezime.getText().toString());
                s.setBrojIndexa(brIndexa.getText().toString());
                s.setGodStud(godStud.getText().toString());
                studenti.add(s);
                i.putExtra("studenti", studenti);
                startActivity(i);
                finish();

            }
        });
    }

    public void editStudent(){
        ime = findViewById(R.id.editName);
        prezime = findViewById(R.id.editSurname);
        brIndexa = findViewById(R.id.editbrindexa);
        godStud = findViewById(R.id.editGodStud);
        confirm = findViewById(R.id.confirmButton);
        forfeit = findViewById(R.id.forfeitButton);

        s = studenti.get(indeks);

        ime.setText(s.getIme());
        prezime.setText(s.getPrezime());
        brIndexa.setText(s.getBrojIndexa());
        godStud.setText(s.getGodStud());
        i = new Intent(EditStudent.this, MainActivity.class);
        forfeit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                i.putExtra("studenti", studenti);
                startActivity(i);
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                studenti.get(indeks).setIme(ime.getText().toString());
                studenti.get(indeks).setPrezime(prezime.getText().toString());
                studenti.get(indeks).setBrojIndexa(brIndexa.getText().toString());
                studenti.get(indeks).setGodStud(godStud.getText().toString());
                i.putExtra("studenti", studenti);
                startActivity(i);

            }
        });



    }
}
